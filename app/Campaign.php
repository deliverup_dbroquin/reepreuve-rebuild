<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Campaign extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_campagne';

    protected $primaryKey = [
        'camp_date', 'camp_type', 'camp_codecentre', 'camp_codeprop',
        'camp_codemodele', 'camp_numbouteille'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
