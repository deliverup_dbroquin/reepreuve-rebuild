<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Log extends Model
{
   // use HasCompositePrimaryKey;

    protected $table = 'epr_log';

    protected $primary = 'log_id';

    protected $primaryKey = 'log_id';

    protected $guarded = [];

    public $timestamps = false;
}
