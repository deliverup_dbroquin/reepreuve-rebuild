<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class TmpConsolidation extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_temp_consolidation_pvreforme';

    protected $primary = [
        'tcpv_login', 'tcpv_campagne_type', 'tcpv_campagne_date',
        'tcpv_campagne_codeprop', 'tcpv_campagne_codecentre', 'tcpv_campagne_numbouteille',
        'tcpv_modele_code'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
