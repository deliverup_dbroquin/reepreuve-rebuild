<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Contact extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_contact_proprietaire';

    protected $primary = 'proc_num';

    protected $guarded = [];

    public $timestamps = false;
}
