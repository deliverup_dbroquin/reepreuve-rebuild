<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class TmpCentre extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_temp_pvreforme_centre';

    protected $primary = [
        'tpvc_login', 'tpvc_datedebut', 'tpvc_datefin',
        'tpvm_codecentre', 'tcam_codeprop', 'tcam_codemodele'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
