<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Root;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Model extends Root
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_modele';

    protected $primary = 'mod_code';

    protected $guarded = [];

    public $timestamps = false;
}
