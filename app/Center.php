<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Center extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_centre';

    protected $primary = 'ctre_code';

    protected $guarded = [];

    public $timestamps = false;
}
