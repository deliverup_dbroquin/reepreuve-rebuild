<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Manufacturer extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_fabricant';

    protected $primary = 'fab_code';

    protected $guarded = [];

    public $timestamps = false;
}
