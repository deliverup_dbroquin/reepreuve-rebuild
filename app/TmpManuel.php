<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class TmpManuel extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_temp_pvreforme_manuel';

    protected $primary = [
        'tpvm_login', 'tpvm_codemodele', 'tpvm_annee',
        'tpvm_mois', 'tpvm_codefabricant'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
