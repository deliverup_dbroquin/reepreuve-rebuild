<?php

use Carbon\Carbon;

/**
    * Get date for researches in KNK format
    * Use --debug option to only get 2014
    *
    * @param boolean $debug
    * @param boolean $isDatetime
    * @return string
 */
function generateDate($debug = false, $isDatetime = false): string
{
    $date = '';

    if($debug) {
        $now = Carbon::parse('2014-3-19')->year;
        $date = $now . '1232';
    } else {
        $date = now()->subYear(1)->year.'0101';
    }

    #if($isDatetime) $date = $date . '000000';

    return ($isDatetime) ? $date . '000000' : $date;
}