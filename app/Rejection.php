<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Rejection extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_motif_ref';

    protected $primary = 'moref_code';

    protected $guarded = [];

    public $timestamps = false;
}
