<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Counter extends Model
{
    protected $table = 'epr_compteurs';

    protected $guarded = [];

    public $timestamps = false;
}
