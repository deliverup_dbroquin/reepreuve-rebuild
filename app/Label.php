<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Label extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_libelle';

    protected $primary = 'lib_code';

    protected $guarded = [];

    public $timestamps = false;
}
