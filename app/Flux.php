<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Flux extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_flux';

    protected $primary = [
        'flux_codecentre', 'flux_destinataire', 'flux_type'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
