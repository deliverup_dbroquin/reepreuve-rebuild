<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Operator extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_operateur';

    protected $primary = 'oper_code';

    protected $guarded = [];

    public $timestamps = false;
}
