<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Renumbering extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_renumerotation';

    protected $primary = ['REN_CodeCentre', 'REN_CodePoste'];

    protected $guarded = [];

    public $timestamps = false;
}
