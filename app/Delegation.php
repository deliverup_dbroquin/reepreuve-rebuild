<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Delegation extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_delegation_signature';

    protected $primary = 'deleg_id';

    protected $guarded = [];

    public $timestamps = false;
}
