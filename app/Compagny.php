<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Compagny extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_societe';

    protected $primary = 'soc_code';

    protected $guarded = [];

    public $timestamps = false;
}
