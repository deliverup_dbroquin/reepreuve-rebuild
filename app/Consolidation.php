<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Consolidation extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_consolidation_pvreforme';

    protected $primaryKey = [
        'cpv_numpvref2', 'cpv_date', 'cpv_index'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
