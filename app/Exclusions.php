<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Exclusions extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_liste_exclusion';

    protected $primary = 'lex_id';

    protected $guarded = [];

    public $timestamps = false;
}
