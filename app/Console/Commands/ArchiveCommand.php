<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use App\Campaign;
use App\Center;
use App\Counter;
use App\Delegation;
use App\Contact;
use App\Edition;
use App\Manufacturer;
use App\Flux;
use App\Model;
use App\Rejection;
use App\Reparation;
use App\Operator;
use App\Characteristics;
use App\OwnerModel;
use App\Owner;
use App\Renumbering;
use App\Compagny;
use App\Label;
use App\Exclusions;
use App\Log;
use App\Consolidation;
use Illuminate\Support\Facades\DB;

class ArchiveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:archive {--debug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate archive from past years';

    /**
     * Last year date in KNK format
     *
     * @var string
     */
    protected $date = '';

    /**
     * Referential related models
     *
     * @var array
     */
    private $referentials = [
        'centers' => Center::class,
        'counters' => Counter::class,
        'delegations' => Delegation::class,
        'contacts' => Contact::class,
        'editions' => Edition::class,
        'manufacturers' => Manufacturer::class,
        'flux' => Flux::class,
        'labels' => Label::class,
        'exclusions' => Exclusions::class,
        'models' => Model::class,
        'rejections' => Rejection::class,
        'reparations' => Reparation::class,
        'operators' => Operator::class,
        'characteristics' => Characteristics::class,
        'ownerModels' => OwnerModel::class,
        'owners' => Owner::class,
        'renumberings' => Renumbering::class,
        'compagnies' => Compagny::class
    ];

    /**
     * Insertions and deletions related models
     *
     * @return void
     */
    private $inserts = [
        'campaigns' => [
            'table' => 'epr_campagne',
            'model' => Campaign::class,
            'field' => 'camp_date',
            'primaries' => [
                'camp_date', 'camp_type', 'camp_codecentre', 'camp_codeprop',
                'camp_codemodele', 'camp_numbouteille'
            ]
        ],
        'consolidation' => [
            'table' => 'epr_consolidation_pvreform',
            'model' => Consolidation::class,
            'field' => 'cpv_date'
        ]
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        // First, store research date
        $this->date = generateDate($this->option('debug'));

        // Launch specific archives generations
        $this->archiveInserts();

        // Launch referential archives generations
        foreach($this->referentials as $name => $referential) {
            $this->archiveReferential($name, $referential);
        }

        $this->info('Archive complete!');
    }

    /**
     * Find and add inserts from production database to archive database
     *
     * @return void
     */
    private function archiveInserts(): void
    {
        foreach($this->inserts as $name => $insert) {
            $this->info(ucfirst($name) .' archives init');

            // Instanciate model
            $model = new $insert['model'];

            // Fetch item item older than last year
            $items = $model->where($insert['field'], '<', $this->date)->get();

            $total = count($items);

            // Generate loading bar
            $bar = $this->output->createProgressBar($total);

            foreach($items->toArray() as $item) {

                // Check if item exist in archive db and create it if not
                if($this->checkPrimaries($model, $insert['primaries'], $item)) {
                    $model::on('archives')->firstOrCreate($item);
                }

                $bar->advance();
            }

            $bar->finish();

            $this->info(ucfirst($name) . ' has been successfully archived!');

            // Trigger campaigns deletion on prod
            $this->deleteInserted($name, $items, $total);
        }
    }

    /**
     * Check if item exist in Archive
     *
     * @param Model $model
     * @param Array $primaries
     * @param Model $item
     * @return void
     */
    private function checkPrimaries($model, $primaries, $item) {
        $toSearch = [];

        foreach($primaries as $primary) {
            $toSearch[$primary] = $item[$primary];
        }

        return $model->on('archives')->where($toSearch)->get()->isEmpty();
    }

    /**
     * Delete inserted items in production database
     *
     * @param string $name
     * @param Collection $items
     * @param integer $total
     * @return void
     */
    private function deleteInserted(String $name, Collection $items, $total): void
    {
        $name = ucfirst($name);

        $this->info($name . ' deletion on production init');

        // Generate loading bar
        $bar = $this->output->createProgressBar($total);

        foreach($items as $item) {
            $item->delete();
            $bar->advance();
        }

        $bar->finish();
        $this->info($name . ' deletion on production has been finished');
    }

    /**
     * Archive referential
     *
     * @param string $type
     * @return void
     */
    private function archiveReferential(string $name, string $type): void
    {
        // Init model
        $model = new $type;

        // Format referential name
        $name = ucFirst($name);

        // Remove all referential in archive
        $this->truncate($model);
        $this->info($name . ' referential has been successfully removed from archive database' . PHP_EOL);

        // (re)create archive
        $items = $model->all();

        $progress = $this->output->createProgressBar(count($items));

        foreach($items->toArray() as $center) {
            $model->on('archives')->firstOrCreate($center);
            $progress->advance();
        }

        $progress->finish();

        $this->info(PHP_EOL . $name . ' referential has been successfully archived!');
    }

    /**
     * Truncate model's table on archive database
     *
     * @param Model $model
     * @return void
     */
    private function truncate($model): void
    {
        $model->on('archives')->truncate();
    }
}
