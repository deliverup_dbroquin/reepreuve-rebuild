<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Log;

class PurgeLogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purge:logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge logs from past years';

   /**
     * Last year date in KNK format
     *
     * @var string
     */
    protected $date = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Store limit date
        $this->date = generateDate(false, true);

        // Launch purges
        $this->purgeLogTable();
    }

    private function purgeLogTable(): void
    {
        $this->info('Purge log table');

        $logs = Log::select('log_id')->where('log_dateheure', '<', $this->date)->get();
        Log::destroy($logs->pluck('log_id')->toArray());

        $this->info('Purge log table complete!');
    }
}
