<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TmpCampaign;
use App\TmpCentre;
use App\TmpConsolidation;
use App\TmpManuel;
use Illuminate\Support\Facades\DB;

class PurgeTmpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purge:tmps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge tmp tables';

    /**
     * Last year date in KNK format
     *
     * @var string
     */
    protected $date = '';

    protected $tmps = [
        'campagne' => [
            'table' => 'epr_temp_campagne',
            'field' => 'tcam_date',
            'model' => TmpCampaign::class
        ],
        'consolidation' => [
            'table' => 'epr_temp_consolidation_pvreforme',
            'field' => 'tcpv_campagne_date',
            'model' => TmpConsolidation::class
        ],
        'centre' => [
            'table' => 'epr_temp_pvreforme_centre',
            'field' => 'tpvc_datedebut',
            'model' => TmpCentre::class
        ],
        'manuel' => [
            'table' => 'epr_temp_pvreforme_manuel',
            'field' => 'tpvm_date',
            'model' => TmpManuel::class
        ],
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        // Store date in knk format
        $this->date = generateDate();

        // Launch purges
        $this->performPurge();
    }

    /**
     * Iterate through tmps table and delete content < last year
     *
     * @return void
     */
    private function performPurge(): void
    {
        foreach($this->tmps as $name => $tmp) {
            $model = new $tmp['model'];

            DB::table($tmp['table'])->where($tmp['field'], '<', $this->date)->delete();

            $this->info(ucfirst($name) . ' tmp has been successfully purged!');
        }
    }
}
