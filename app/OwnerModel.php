<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class OwnerModel extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_prop_modele';

    protected $primary = ['promo_codeproprietaire', 'promo_codemodele'];

    protected $guarded = [];

    public $timestamps = false;
}
