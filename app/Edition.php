<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edition extends Model
{
    protected $table = 'epr_edition_activite';

    protected $guarded = [];

    public $timestamps = false;
}
