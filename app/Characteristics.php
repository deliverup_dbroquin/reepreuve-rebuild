<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Characteristics extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_prop_carac';

    protected $primary = ['poca_codeproprietaire', 'poca_codemodele', 'poca_codemarque'];

    protected $guarded = [];

    public $timestamps = false;
}
