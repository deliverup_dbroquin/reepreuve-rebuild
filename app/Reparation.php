<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Reparation extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_motif_rep';

    protected $primary = 'morep_code';

    protected $guarded = [];

    public $timestamps = false;
}
