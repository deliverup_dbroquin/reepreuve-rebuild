<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Owner extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_proprietaire';

    protected $primary = 'prop_code';

    protected $guarded = [];

    public $timestamps = false;
}
