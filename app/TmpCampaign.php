<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class TmpCampaign extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'epr_temp_campagne';

    protected $primary = [
        'tcam_login', 'tcam_date', 'tcam_type',
        'tpvm_codecentre', 'tcam_codeprop', 'tcam_codemodele',
        'tcam_numbouteille'
    ];

    protected $guarded = [];

    public $timestamps = false;
}
